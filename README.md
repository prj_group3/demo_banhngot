# Demo_BanhNgot

*The pastry shop is a project inspired by Artemis bakery, with the aim of simplifying the management of the store, staff and orders*


## Installation

- Java JDK 8
- Apache Tomcat 10.0
- Database SQL server 2019.
- Apache Netbeans IDE 13

## Technologies/Frameworks Used

- Front-end: HTML, CSS, Javascript, Boostrap 5
- Backed-end: Java - JSP/Servlet 
- Database: SQL



## 

1. Homepage

![img](img/Hompage1.png)
##
![img](img/Homepage2.png)
##
![img](img/Homepage3.png)
##
![img](img/Homepage4.png)
##
![img](img/Homepage5.png)

2. View products

![img](img/Product(1).png)
![img](img/Product(2).png)
![img](img/Product(3).png)

3. View Cart

![img](img/GioHang(5).png)
![img](img/GioHang(6).png)

4. Pay page

![img](img/ThanhToan.png)

5. View order status

![img](img/KiemHang(2).png)
![img](img/KiemHang(3).png)
![img](img/KiemHang(1).png)

6. Manager products

![img](img/ManagerProduct(1).png)
![img](img/ManagerProduct(2).png)

6.1 Insert products

![img](img/KiemHang(4).png)

6.2 Update products

![img](img/UpdateItem.png)

6.3 Delete products

![img](img/DeleteItem(2).png)
![img](img/DeleteItem(3).png)


7. Account Management

![img](img/ManagerAccount.png)

8. Application management

![img](img/QuanLyDon(2).png)
![img](img/QuanLyDon(1).png)

9. Revenue

![img](img/QuanLyDoanhThu.png)












